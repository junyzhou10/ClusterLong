% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DendroPlot.R
\name{DendroPlot}
\alias{DendroPlot}
\title{Plot dendrogram}
\usage{
DendroPlot(Cluster.object, No.Cluster = NULL, CH = FALSE, plain = FALSE, ...)
}
\arguments{
\item{Cluster.object}{The object return from function LongDataCluster}

\item{No.Cluster}{User specified number of clusters}

\item{CH}{If TRUE, iuse optimal number of clusters determined by CH index to cut dendrogram. The default is to use Gapb}

\item{plain}{If TRUE, the dendrogram only, without coloring the clustering results}

\item{...}{Additional arguments for plot}
}
\description{
Plot dendrogram
}
\section{Functions}{
\itemize{
\item \code{DendroPlot}: on output from LongDataCluster, yield the corresponding dendropgram
}}

\examples{
output = LongDataCluster(Longdat$Dat$obs,
Longdat$Dat[,paste("y", seq(5), sep = "_")],
Longdat$Dat$id)
DendroPlot(output)

}
